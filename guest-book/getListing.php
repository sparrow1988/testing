<?php

require "config/database.php";
require "Database.php";

//PDO:

$db = new Database();

$sth = $db->prepare("SELECT c.id, u.name, c.comment, c.date
                                 FROM user u, comment c
                                 WHERE c.user_id = u.id");
$sth->setFetchMode(PDO::FETCH_ASSOC);
$sth->execute();
$data = $sth->fetchAll();
echo json_encode($data);


