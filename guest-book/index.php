<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="bootstrap-3.3.6-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
<div class="row">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1>Guest Book</h1>
        </div>
    </div>
</div>

    <div class="container">

        <div class="form well">

            <!--Form-->
            <form action="#" role="form">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" placeholder="Name" id="name">
                </div>

                <div class="form-group">
                    <label for="email">Your email</label>
                    <input type="email" placeholder="email" id="email" class="form-control">
                </div>

                <div class="form-group">
                    <label for="message">Message Text</label>
                    <textarea name="message" id="message" cols="30" rows="10" placeholder="message" class="form-control"></textarea>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                    <div class="col-sm-2 col-sm-offset-8">
                        <button type="submit" class="btn btn-primary">Add Picture</button>
                    </div>
                </div>

            </form>

            <!--Form-->
        </div>

        <div class="row">
            <hr />
        </div>

        <!--Comments-->
        <div id="comments">
        <div class="panel-group">
            <div class="row">

                <!--Comment-->
<!--                <div class="panel panel-primary">-->
<!--                    <div class="panel-heading">-->
<!--                        <div class="col-sm-2">UserName</div>-->
<!--                        <div class="col-sm-2 col-sm-offset-8">Date</div>-->
<!--                        <div class="clearfix"></div>-->
<!--                    </div>-->
<!--                    <div class="panel-body">Message</div>-->
<!--                    <div class="panel-footer">-->
<!--                        <div class="col-sm-2">-->
<!--                            <button type="submit" class="btn btn-primary">Show More...</button>-->
<!--                        </div>-->
<!--                        <div class="col-sm-2 col-sm-offset-8">-->
<!--                            <button type="submit" class="btn btn-primary">Delete</button>-->
<!--                        </div>-->
<!--                        <div class="clearfix"></div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
                <!--Comment-->

            </div>
        </div>

        <!--Pagination-->
        <div class="row text-center">
            <ul class="pagination">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&raquo;</a></li>
            </ul>
        </div>
        <!--Pagination-->

    </div>

<!--Footer-->
<div class="row">
    <div class="panel panel-primary">
        <div class="panel-heading">Keith Chasen</div>
        <div class="panel-body">
                <div class="col-sm-11">&copy;</div>
                <div class="col-sm-1">2016</div>
        </div>
    </div>
</div>

</body>
<script src="js/jquery.js"></script>
<script src="js/script.js"></script>

</html>