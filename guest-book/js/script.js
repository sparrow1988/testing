$(document).ready(function () {
    $.get('getListing.php', function (e) {

        //Parsing data from php/DB to array
        var arr = JSON.parse(e);

        var firstKey, secondArr ,secondKey, id, name, comment, date;
        for (firstKey in arr){
            secondArr = arr[firstKey];
            for (secondKey in secondArr){
                switch (secondKey) {
                    case "id" :
                        id = secondArr[secondKey];
                    break;
                    case 'name':
                        name = secondArr[secondKey];
                        break;
                    case "comment":
                        comment = secondArr[secondKey];
                        break;
                    case "date":
                        date = secondArr[secondKey];
                        break;
                }

            }
            //alert("ID: " + id + " Name: " + name + " Message: " + comment + " Date: " + date);
            $("#comments .panel-group .row").append("<div class='panel panel-primary'>"
                                                    + "<div class='panel-heading'>"
                                                    + "<div class='col-sm-2'>" + name + "</div>"
                                                    + "<div class='col-sm-2 col-sm-offset-8'>" + date + "</div>"
                                                    + "<div class='clearfix'></div></div>"
                                                    + "<div class='panel-body'>" + comment + "</div>"
                                                    + "<div class='panel-footer'>"
                                                    + "<div class='col-sm-2'>"
                                                    + "<button type='submit' class='btn btn-primary'>"
                                                    + "Show More...</button></div>"
                                                    + "<div class='col-sm-2 col-sm-offset-8'>"
                                                    + "<button type='submit' class='btn btn-primary'>Delete</button></div>"
                                                    + "<div class='clearfix'></div>"
                                                    + "</div></div>");
        }
    });
});

